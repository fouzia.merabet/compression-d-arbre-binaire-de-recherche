


let rec search_l l n = match l with
    [] -> false
  | (v,k)::q -> if v = n then true else search_l q n;;

(* si je tombe sur un noeud avec une seule valeur, je fait un parcours d'arbre classique, si c'est un noeud avec plusieurs elements, j'envoie la liste vers "search_l" qui va parcourir la liste pour savoir si l'element est dedans*)

let rec find_ctree_list l path = match l with
    [] -> 0
   |(etiq,key)::q -> if not (none_or_some key) then
                       if (List.length path) = 0 then etiq
                       else
                         find_ctree_list q path
                     else
                       if (option_to_tlist key) = path then etiq
                       else
                         find_ctree_list q path;;



let ctree_list_search abr n =
  let rec ctree_list_search_aux abr path = match abr with
      Leaf_ -> (List.iter (fun x -> (print_endline (string_of_int x))) path);false
    | Node_(el,(g,k1),(d,k2)) -> let v = !el in if (List.length v = 1) then let (etiq,key) = (List.hd v) in
                                                                          if n = etiq then true
                                                                          else
                                                                            if n < etiq then
                                                                              if none_or_some k1 then
                                                                                let new_path = (option_to_t k1) :: path in
                                                                                ctree_list_search_aux g new_path
                                                                              else
                                      
                                                                                ctree_list_search_aux g path
                                                                            else
                                                                              if none_or_some k2 then
                                                                                let new_path = (option_to_t k2) :: path in
                                                                                ctree_list_search_aux d new_path
                                                                              else

                                                                                ctree_list_search_aux d	path
                                                else
                                                  let etiq = find_ctree_list v path in
                                                  if n = etiq then true
                                                  else
                                                    if n < etiq then
                                                      if none_or_some k1 then
                                                        let new_path = (option_to_t k1) :: path in
                                                        ctree_list_search_aux g new_path
                                                      else
                                                        ctree_list_search_aux g path
                                                    else
                                                      if none_or_some k2 then
                                                        let new_path = (option_to_t k2) :: path in
                                                        ctree_list_search_aux d	new_path
                                                      else
                                                        ctree_list_search_aux d	path
  in
  ctree_list_search_aux abr [];;
                                                           
    
 


(* Solution avec un arbre a noeud liste *)

let all_keys = ref [] (*l'ensemble des clés utilisé pour l'arbre courant après compression, qui sert a filtré les clés qui ne devraient pas y etre (algo pas parfait donc on rafistole*)

(*on essaie d'inserer une structure rencontrée, si elle existe déja, on renvoie false pour savoir dans compressed_an_abr qu'on doit mettre une clé sur un chemin*)


let rec try_insert cell abr_compressed = match abr_compressed with
    [] -> let word,el_key = cell in let el,key = el_key in let r = ref [(el,None)] in (true,r,[(word,r)])
   |(w,l_elts)::q -> let word,el = cell in
                     if word = w then
                       (false,(l_elts := !l_elts @ [el];l_elts),((w,l_elts)::q))
                     else
                       let b,r,tab = try_insert cell q in (b,r,(w,l_elts)::tab);;


(*c'est la fonction pour différencier les noeuds normaux des noeuds rouge (noeux rouge = pointant vers une liste des elements compréssé*)

let what_kind_of_node g d cur ptr word_set key = let kg,kd = key in let is_key_g,sag = g in let is_key_d,sad = d in match is_key_g,is_key_d,cur with
    false,false,b -> ((not b),Node_(ptr,(sag,None),(sad,None)),word_set,kd)
  | true,true,b ->  all_keys := kg :: !all_keys; all_keys := kd :: !all_keys; ((not b),Node_(ptr,(sag,(Some(kg))),(sad,(Some(kd)))),word_set,kd)
  | true,false,b -> all_keys := kg :: !all_keys; ((not b), Node_(ptr,(sag,(Some(kg))),(sad,None)),word_set,kd)
  | false,true,b -> all_keys := kd :: !all_keys; ((not b), Node_(ptr,(sag,None),(sad,(Some(kd)))),word_set,kd)


(*parcours de l'arbre initial et création de la liste des mots rencontré + de l'arbre compréssé*)

let ctree_list_compute ab =
  let rec ctree_list_compute_aux a tab_refs key cpt =
    match a with
      Leaf -> (false,Leaf_,tab_refs,cpt)
    | Node(el,sag,sad) as node -> let start = cpt+1 in let is_key_g,g,t1,c1 = ctree_list_compute_aux sag  tab_refs ((cpt+1) <:::> key) (cpt+1) in
                                                     let is_key_d,d,t2,c2 = ctree_list_compute_aux sad t1 ((c1+1) <:::> key) (c1+1) in
                                                     let ed = c1+1 in 
                                                     let b,r,tab = try_insert ((phi node),(el,key)) t2 in
                                                     what_kind_of_node (is_key_g,g) (is_key_d,d) b r tab (start,ed) in 
  ctree_list_compute_aux ab [] (Some([])) 0;;

let ctree_list abr =
  all_keys := [];
  let b,tree,l_refs,c = ctree_list_compute abr in (tree,(filter_bad_keys l_refs delifnotcontain_l));;


