type 'a abr = Leaf
            | Node of 'a * 'a abr * 'a abr;;
type 'a abr_comp = Leaf_
                 | Node_ of 'a * ('a abr_comp * int option) * ('a abr_comp * int option);;



let rec search_abr n abr = match abr with
    Leaf -> false
   |Node(el,g,d) -> if n = el
                    then true
                    else
                      if n < el
                      then search_abr n g
                      else search_abr n d;;


let gen_abr l =
  let rec gen_abr_aux l abr = match l,abr with
      [],a -> a
    | t::q,Leaf -> gen_abr_aux q (Node(t,Leaf,Leaf))
    | t::q,Node(el,sag,sad) -> if t < el then gen_abr_aux q (Node(el,(gen_abr_aux [t] sag),sad))
                               else
                                 if t == el then gen_abr_aux q (Node(el,sag,sad))
                                 else
                                   gen_abr_aux q (Node(el,sag,(gen_abr_aux [t] sad)))
  in
  gen_abr_aux l Leaf;;





let print_abr abr =
  let rec print indent tree =
    match tree with
       Leaf -> 
        Printf.printf "%s*\n" indent
     | Node (root, sag, sad) ->
        Printf.printf "%s----\n" indent;
        print (indent ^ "| ") sad;
        Printf.printf "%s%d\n" indent root;
        print (indent ^ "| ") sag;
        Printf.printf "%s----\n" indent
  in
  print "" abr;;

let none_or_some el = match el with
    None -> false
  | Some(i) -> true;;

let option_to_tlist el = match el with
    None -> []
  | Some(i) -> i;;

let option_to_t el = match el with
    None -> 0
  | Some(i) -> i;;

(*fonction phi qui donne le mot associé a un ABR*)
let rec phi abr = match abr with
    Leaf -> ""
  | Node(el,sag,sad) -> "(" ^ phi(sag) ^ ")" ^ phi(sad);;


let (<:::>) i i_opt = match i_opt with
    None -> None
  |Some(l) -> Some(i::l);;

let rec isnotcontain l el = match l with
    [] -> true
   |t :: q -> if t = el then false else isnotcontain q el ;;

let rec foreach_p l p = match l with
    [] -> []
  | t :: q -> if p t then foreach_p q p else t :: (foreach_p q p);;

let rec delifnotcontain_l l keys = match l with
    [] -> l
  | (el,ll)::q -> if none_or_some ll then (el,Some(foreach_p (option_to_tlist ll) (isnotcontain keys))) :: delifnotcontain_l q keys else (el,ll) :: delifnotcontain_l q keys;;

let print_m k v = print_endline ((List.fold_right (fun x acc -> (string_of_int x) ^ acc) k "")^" "^(string_of_int v));;

let rec delifnotcontain_m map ks = let (new_map,kss) = CtreeMap.fold (fun k v m_keys -> let (m,keys) = m_keys in (CtreeMap.add (foreach_p k (isnotcontain keys)) v m,keys)) map (CtreeMap.empty,ks) in new_map;; 

let rec filter_bad_keys l_refs f = match l_refs with
    [] -> []
  | (w,l) :: q -> (w,(l := (f !l !all_keys);l)) :: (filter_bad_keys q f);;




