open Abr;;

module CtreeMap = Map.Make(struct type t = int list let compare = compare end);;


let rec try_insert_map cell abr_compressed = match abr_compressed with
    [] -> let word,el_key = cell in let el,key = el_key in let r = ref (CtreeMap.add [] el (CtreeMap.empty))  in (true,r,[(word,r)])
   |(w,map_elts)::q -> let word,el_key = cell in
                       let el,key = el_key in
                       if word = w then
                       (false,(map_elts := (CtreeMap.add key el !map_elts);map_elts),((w,map_elts)::q))
                     else
                       let b,r,tab = try_insert_map cell q in (b,r,(w,map_elts)::tab);;


let ctree_map_compute ab =
  let rec  ctree_map_compute_aux a tab_refs key cpt =
    match a with
      Leaf -> (false,Leaf_,tab_refs,cpt)
    | Node(el,sag,sad) as node -> let start = cpt+1 in let is_key_g,g,t1,c1 =  ctree_map_compute_aux sag  tab_refs ((cpt+1) :: key) (cpt+1) in
                                                     let ed = c1 +1 in
                                                     let is_key_d,d,t2,c2 =  ctree_map_compute_aux sad t1 ((c1+1) :: key) (c1+1) in
                                                     let b,r,tab = try_insert_map ((phi node),(el,key)) t2 in
                                                     what_kind_of_node (is_key_g,g) (is_key_d,d) b r tab (start,ed) in
  ctree_map_compute_aux ab [] [] 0;;




(*on renvoie la liste des (mot,map) et la structure d'arbre, a eux deux ils représentent l'arbre compréssé*)
let ctree_map abr =
  all_keys := [];
  let b,tree,l_refs,c = ctree_map_compute  abr in let rfs = filter_bad_keys l_refs delifnotcontain_m in (tree,rfs);;

exception Ctree_map_not_found;;


let ctree_map_search tree n =
  let rec ctree_ms_aux ctreem path = match ctreem with
      Leaf_ -> false
    | Node_(map,(g,k1),(d,k2)) -> let etiq = CtreeMap.find path !map in
                                  if n = etiq then true
                                  else
                                    if n < etiq then
                                      if none_or_some k1 then
                                        let new_path = (option_to_t k1) :: path in
                                        ctree_ms_aux g new_path
                                      else
                                        ctree_ms_aux g path
                                    else
                                      if none_or_some k2 then
                                        let new_path = (option_to_t k2) :: path in
                                        ctree_ms_aux d new_path
                                      else
                                        ctree_ms_aux d path in
  ctree_ms_aux tree [];;
