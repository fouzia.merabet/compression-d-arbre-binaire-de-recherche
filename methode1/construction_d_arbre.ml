(*-                 	strecture d'arbre	                *)     


type 'a arbre =
| Empty | Node of ('a * 'a arbre * 'a arbre)


(*                   Construction de l'arbre                  *)     


let rec construire l = 

  let rec insert v = function
  | Empty -> Node (v, Empty, Empty); 
  | Node (r, fg, fd) -> 
    if v < r then Node (r, insert v fg, fd)
    else Node ( r, fg , insert v fd)
  in match l with 
  [] -> Empty
  | t::q -> 
  insert t (construire q);;
  

let construire_arbre l = 
	let li = List.rev l in 
	construire li;;
