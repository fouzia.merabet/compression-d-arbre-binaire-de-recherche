
(*				calculer pére 				*)
 
let funpere x  = (x-( x mod 2))/ 2 ;;


(*             fonction indices            *)

let rec existe a l =
	match l with 
	[]->false
	|(c,b)::xs-> existe a xs
	|(a,b)::xs->true ;;

let rec existe_indice a l =
	match l with 
	[]->a
	|(c,b)::xs-> existe_indice a xs
	|(a,b)::xs->b ;;
(*             ajout des indices            *)
(*             liste des indices            *)
 let rec associate a b lb=
	match b with
	[]->lb
	|x::y-> lb:= !lb@ [(a,x)];
	       associate a y lb  ;;
	
let rec liste_indice la lb=
    match la with
    [] -> lb;
    |x::y-> match x with
    []->  lb
    |a::b -> let lb =associate a b lb in 
    liste_indice y lb;;
    

(*                    creer liste pere et fils               *)


let rec fun_indice l lpg lpd lf =
	match l with 
	[]-> [];
	|(a,b)::y -> if (not (existe (funpere b) lf))then(
					if (b mod 2 = 1)then(
						lpd:= !lpd @ [funpere b];
						)
						else(
						lpg:= !lpg @[funpere b]
						)
						lf:= !lf @(b,'b')
			     	)
	        
			else( if (b mod 2 = 1)then(
						lf:= !lf @(b,existe_indice b lf @'a')
						)
						else (
						lf:= !lf @(b,existe_indice b lf)
						)
			     	)
			
			
