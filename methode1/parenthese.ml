(*               	génération de parenthéses                 *)

let rec parenthese a=
	match a with
	Empty -> ""
	| Node (r, fg, fd) -> "(" ^ parenthese(fg) ^ ")" ^ parenthese(fd);;
	


let rec decompose a =
	let rec decompose1 a1 i1 l1  =
		match a1 with 
		 Empty -> print_string "" ;
		| Node (r, fg, fd) -> l1 := !l1 @ [(i1,r)] ;
							  decompose1 fg (i1*2) l1;
							  decompose1 fd (i1*2+1) l1;
  
    in 
    let l = ref [] in
	decompose1 a 1 l;
	l;; 


let rec decompose_parenthese a =
	let rec decompose1_parenthese a1 i1 l1  =
		match a1 with 
		 Empty -> print_string "" ;
		| Node (r, fg, fd) -> l1 := !l1 @ [(i1,parenthese a1 )] ;
							  decompose1_parenthese fg (i1*2) l1;
							  decompose1_parenthese fd (i1*2+1) l1;
  
    in 
    let l = ref [] in
	decompose1_parenthese a 1 l;
	l;; 


