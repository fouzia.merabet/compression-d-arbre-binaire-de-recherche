(*       fonctions d'affichage         *)
let rec print_list l =
	match l with 
	[] -> "";
	|x::xs ->
		print_string "(";
		print_int (fst x);
		print_string " , ";
		print_int (snd x);
		print_string ")";
		print_list xs
			  ;;
			 
let rec print_elt l=
    match l with 
	[] -> "";
	|x::xs ->  
		print_string "   [";
		print_int x;
		print_string "   ;";
		print_elt xs
	         
			  ;;

let  rec print_indice l =
	match l with 
	[] -> "";
	|(x,y)::xs -> 
		print_string "   (";
		print_int  x;
		print_string "   ,";
		print_int  y;
		print_string "   )";
		print_indice  xs
	           
			  ;;

let print_map key value =
    print_string "\n";
    print_string (key);
    print_string "   [";
    List.iter print_int value;
	print_string "   ]";;
	
