let extraction_alea l p = 
	match l with 
	[] -> (l ,p)  
	|x::s -> let len = List.length(l) in
			let i = Random.int len in
			let e = List.nth l i in
			let pp = List.append [e] p in
			let ll = List.filter (fun x -> x != e ) l in
			(ll,pp)
    
let gen_permutation n =
  let l = ref [] in
    for i=1 to n do
      l := List.append !l [i]
    done;
    let p = ref [] in
    for i=1 to n do
      let (ll,pp) = extraction_alea !l !p in
      p:= pp ;
      l:=ll
    done;
    p;;
