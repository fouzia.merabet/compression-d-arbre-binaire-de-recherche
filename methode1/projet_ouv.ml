
(*               génération aléatoire         *)     

let extraction_alea l p = 
	match l with 
	[] -> (l ,p)  
	|x::s -> let len = List.length(l) in
			let i = Random.int len in
			let e = List.nth l i in
			let pp = List.append [e] p in
			let ll = List.filter (fun x -> x != e ) l in
			(ll,pp)
    
let gen_permutation n =
  let l = ref [] in
    for i=1 to n do
      l := List.append !l [i]
    done;
    let p = ref [] in
    for i=1 to n do
      let (ll,pp) = extraction_alea !l !p in
      p:= pp ;
      l:=ll
    done;
    p;;



(*-                 	strecture d'arbre	                *)     


type 'a arbre =
| Empty | Node of ('a * 'a arbre * 'a arbre)


(*                   Construction de l'arbre                  *)     


let rec construire l = 

  let rec insert v = function
  | Empty -> Node (v, Empty, Empty); 
  | Node (r, fg, fd) -> 
    if v < r then Node (r, insert v fg, fd)
    else Node ( r, fg , insert v fd)
  in match l with 
  [] -> Empty
  | t::q -> 
  insert t (construire q);;
  


let construire_arbre l = 
	let li = List.rev l in 
	construire li;;
  

(*               	génération de parenthéses                 *)

let rec parenthese a=
	match a with
	Empty -> ""
	| Node (r, fg, fd) -> "(" ^ parenthese(fg) ^ ")" ^ parenthese(fd);;
	


let rec decompose a =
	let rec decompose1 a1 i1 l1  =
		match a1 with 
		 Empty -> print_string "" ;
		| Node (r, fg, fd) -> l1 := !l1 @ [(i1,r)] ;
							  decompose1 fg (i1*2) l1;
							  decompose1 fd (i1*2+1) l1;
  
    in 
    let l = ref [] in
	decompose1 a 1 l;
	l;; 


let rec decompose_parenthese a =
	let rec decompose1_parenthese a1 i1 l1  =
		match a1 with 
		 Empty -> print_string "" ;
		| Node (r, fg, fd) -> l1 := !l1 @ [(i1,parenthese a1 )] ;
							  decompose1_parenthese fg (i1*2) l1;
							  decompose1_parenthese fd (i1*2+1) l1;
  
    in 
    let l = ref [] in
	decompose1_parenthese a 1 l;
	l;; 




module Mymap = Map.Make(String) ;;
module Mymap2 = Map.Make(Int64) ;;


let rec toMap l m =
	match l with
	[] -> m;
	|(x, y)::s -> if (not(Mymap.exists (fun k va -> k =y) m)) then(
					let m= Mymap.add y [x] m in
					toMap s m;
				)
				 else (
					let v = Mymap.find y m in 
					let m = Mymap.add y (v@[x]) m in
					toMap s m
				 )
				 

(*				calculer pére 				*)
 
let funpere x  = (x-( x mod 2))/ 2 ;;


(*             fonction indices            *)

let rec existe a l =
	match l with 
	[]->false
	|(c,b)::xs-> existe a xs
	|(a,b)::xs->true ;;

let rec existe_indice a l =
	match l with 
	[]->a
	|(c,b)::xs-> existe_indice a xs
	|(a,b)::xs->b ;;
(*             ajout des indices            *)
(*             liste des indices            *)
 let rec associate a b lb=
	match b with
	[]->lb
	|x::y-> lb:= !lb@ [(a,x)];
	       associate a y lb  ;;
	
let rec liste_indice la lb=
    match la with
    [] -> lb;
    |x::y-> match x with
    []->  lb
    |a::b -> let lb =associate a b lb in 
    liste_indice y lb;;
    

(*                    creer liste pere et fils               *)


let rec fun_indice l lpg lpd lf =
	match l with 
	[]-> [];
	|(a,b)::y -> if (not (existe (funpere b) lf))then(
					if (b mod 2 = 1)then(
						lpd:= !lpd @ [funpere b];
						)
						else(
						lpg:= !lpg @[funpere b]
						)
						lf:= !lf @(b,'b')
			     	)
	        
			else( if (b mod 2 = 1)then(
						lf:= !lf @(b,existe_indice b lf @'a')
						)
						else (
						lf:= !lf @(b,existe_indice b lf)
						)
			     	)
			
			
			
(*       fonctions d'affichage         *)
let rec print_list l =
	match l with 
	[] -> "";
	|x::xs ->
		print_string "(";
		print_int (fst x);
		print_string " , ";
		print_int (snd x);
		print_string ")";
		print_list xs
			  ;;
			 
let rec print_elt l=
    match l with 
	[] -> "";
	|x::xs ->  
		print_string "   [";
		print_int x;
		print_string "   ;";
		print_elt xs
	         
			  ;;

let  rec print_indice l =
	match l with 
	[] -> "";
	|(x,y)::xs -> 
		print_string "   (";
		print_int  x;
		print_string "   ,";
		print_int  y;
		print_string "   )";
		print_indice  xs
	           
			  ;;

let print_map key value =
    print_string "\n";
    print_string (key);
    print_string "   [";
    List.iter print_int value;
	print_string "   ]";;
	
(*------------------Times-------------------*)	

(*open Unix

let time f =
  let t = Unix.gettimeofday () in
  let res = f () in
  Printf.printf "Execution time: %f secondsn"
                (Unix.gettimeofday () -. t);
  res
;;*)

(*-----------	Tests	-----------*)
let l= [4;2;8;1;3;6;9;5;7]in 
let a = construire_arbre l in
let l = decompose a in
let l2 = decompose_parenthese a in
print_list !l;
print_string "\n";
print_list !l2;

funpere 7;

let m = Mymap.empty in
let m2 = toMap !l2 m in
Mymap.iter print_map m2;

let l = Mymap.map (fun i x -> let l=x in l) m2 in 
l
;;


let l= [[1];[3];[2;6];[4;5;12;13;7]] in 
let lb =ref [] in 
liste_indice l lb ;
print_indice !lb;
;;








(*let ll=[] in 
Mymap.iter indice_map m2 ll ;


time (fun() -> Mymap.iter print_map m2);*)


	 
