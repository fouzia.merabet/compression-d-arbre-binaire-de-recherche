let extraction_alea l p =
  Random.init(int_of_float(Unix.time()));
  let length_l = List.length l in
  if(length_l == 0) then (l,p) else
    let r = Random.int length_l in
    let rec extraction_aux i target = match target with
        t::q -> if(i == 0) then
                  (t,q)
                else
                  let (el,l) = extraction_aux (i-1) q in
                  (el,t :: l)
    in let (el,l) = extraction_aux r l in
       (l,el :: p);;

let gen n =
  let rec interval n m = 
  if n > m then 
    [] 
  else
  if n = m then
    [m]
  else
    n :: interval (n + 1) m in
  interval 1 n;;

let gen_permutation n =
  let lgen = gen n in
  let (l,p) = extraction_alea lgen [] in
  let rec gen_permut_aux ll pp = match ll with
      [] -> pp
    | t::q -> let (a,b) = extraction_alea ll pp in gen_permut_aux a b in
  gen_permut_aux l p;;
